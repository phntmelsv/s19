//console.log("Hello World");

// login functionality

let username;
let password;
let role;

// Enter your username
username = prompt("Enter your username");

// Enter your password
password = prompt("Enter your password");

// Enter your role ("student" or "teacher" or "admin")
role = prompt("Enter your role");

// Input must not be empty
function loginCredsComplete(username, password, role) {

	
	// Welcome to the class portal, student!
	// all input fields are empty

	if(username === '' || password === '' || role === '') {
		return 'Input must not be empty';
	}

	else if(username != '' && password != '' && role === 'student') {
		return 'Welcome to the class portal, student!';
	}

	// Thank you for logging in, teacher!
	else if(username != '' && password != '' && role === 'teacher') {
		return 'Thank you for logging in, teacher!';
	}

	// Welcome back to the class portal, admin!
	else if(username != '' && password != '' && role === 'admin') {
		return 'Welcome back to the class portal, admin!';
	}

	// Role out of range
	else if(role != 'admin' || role != 'student' || role != 'teacher') {
		return 'Role out of range';
	}	

	else {
		return 'Input must not be empty';
	}

}

let message = loginCredsComplete(username, password, role);
alert(message);



function checkAverage(numOne, numTwo, numThree, numFour) {
	let average = Math.round((parseInt(numOne) + parseInt(numTwo) + parseInt(numThree) + parseInt(numFour)) / 4);

		switch (true) {
				// >=96
		    case (average > 95): 
		        console.log("Hello, student, your average is: " + average + ". The letter equivalent is A+");
		        break;
		    	// 90-95
		    case (average >= 90 && average <= 95): 
		        console.log("Hello, student, your average is: " + average + ". The letter equivalent is A");
		        break;
		        // 85-89
		    case (average >= 85 && average <= 89):
		        console.log("Hello, student, your average is: " + average + ". The letter equivalent is B");
		        break;
		        // 80-84
		    case (average >= 80 && average <= 84):
		        console.log("Hello, student, your average is: " + average + ". The letter equivalent is C");
		        break;
		        // 75-79
		    case (average >= 75 && average <= 79):
		        console.log("Hello, student, your average is: " + average + ". The letter equivalent is D");
		        break;
		        // <75
		    case (average <= 74):
		        console.log("Hello, student, your average is: " + average + ". The letter equivalent is F");
		        break;
		    default:
		        console.log("Please input a valid grade");
		        break;
		}

}